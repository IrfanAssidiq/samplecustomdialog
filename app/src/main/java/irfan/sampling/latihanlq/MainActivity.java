package irfan.sampling.latihanlq;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Bind(R.id.klik4)
    Button klik4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage("Hai saya Alert Dialog").setCancelable(true).setTitle("Sample")
                .setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent testdialog = new Intent(Intent.ACTION_VIEW);
                        testdialog.setData(Uri.parse("https://tajwid.learn-quran.co/choose-recipients"));
                        startActivity(testdialog);
                    }
                });

        klik4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Test Button Pencet OKE", Toast.LENGTH_SHORT).show();
                dialog.show();
            }
        });
    }

    public void pencet(View v){
        switch (v.getId()){
            case R.id.klik:{
                Toast.makeText(MainActivity.this, "Test Button Pencet", Toast.LENGTH_SHORT).show();
            }
            break;
            case R.id.klik2:{
                Toast.makeText(MainActivity.this, "Test Button Pencet2", Toast.LENGTH_SHORT).show();
            }
            break;
            case R.id.klik3:{
                Toast.makeText(MainActivity.this, "Test Button Pencet3", Toast.LENGTH_SHORT).show();
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.layout_dialog);
                dialog.setTitle("sample");
                dialog.getWindow().setGravity(Gravity.BOTTOM);
                TextView oke = dialog.findViewById(R.id.oke);
                TextView no = dialog.findViewById(R.id.no);
                dialog.setCancelable(false);
                oke.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent testdialog = new Intent(Intent.ACTION_VIEW);
                        testdialog.setData(Uri.parse("https://tajwid.learn-quran.co/choose-recipients"));
                        startActivity(testdialog);
                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Terima Kasih", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        }

    }

    @Override
    public void onClick(View view) {
        Toast.makeText(MainActivity.this, "Test Button Pencet 2", Toast.LENGTH_SHORT).show();
    }
}
